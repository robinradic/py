<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class CrvsModuleClientsCreateClientsFields extends Migration
{

    protected $fields = [
        'advisor'                => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => \Anomaly\UsersModule\User\UserModel::class,
                'mode'    => 'dropdown',
            ],
        ],
        'firstname'              => 'anomaly.field_type.text',
        'lastname'               => 'anomaly.field_type.text',
        'initials'               => 'anomaly.field_type.text',
        'maiden_name'            => 'anomaly.field_type.text',
        'gender'                 => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    'male'   => 'module::field.gender.option.male',
                    'female' => 'module::field.gender.option.female',
                ],
            ],
        ],
        'birthdate'              => [
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'default_value' => null,
                'mode'          => 'date',
                'date_format'   => 'd-m-Y',
                'year_range'    => '-100:0',
                'picker'        => true,
            ],
        ],
        'unknown_birthdate'      => 'anomaly.field_type.boolean',
        'address'                => 'anomaly.field_type.text',
        'housenumber'            => 'anomaly.field_type.text',
        'housenumber_suffix'     => 'anomaly.field_type.text',
        'postcode'               => 'anomaly.field_type.text',
        'city'                   => 'anomaly.field_type.text',
        'nationality'            => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    'nederlandse' => 'module::field.nationality.option.nederlandse',
                    'turks'       => 'module::field.nationality.option.turks',
                    'overige'     => 'module::field.nationality.option.overige',
                ],
            ],
        ],
        'marital_status'         => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    'single'      => 'module::field.marital_status.option.single',
                    'married'     => 'module::field.marital_status.option.married',
                    'not_married' => 'module::field.marital_status.option.not_married',
                    'partnership' => 'module::field.marital_status.option.partnership',
                    'divorced'    => 'module::field.marital_status.option.divorced',
                    'widow'       => 'module::field.marital_status.option.widow',
                ],
            ],
        ],
        'phone_number'           => 'anomaly.field_type.text',
        'mobile_number'          => 'anomaly.field_type.text',
        'email'                  => 'anomaly.field_type.email',
        'bank_account_number'    => 'anomaly.field_type.integer',
        'has_wmo_dossier'        => 'anomaly.field_type.boolean',
        'receive_correspondence' => 'anomaly.field_type.boolean',
        'correspondence_about'   => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    'none' => 'module::field.correspondence_about.option.none',
                    'all'  => 'module::field.correspondence_about.option.all',
                    'only' => 'module::field.correspondence_about.option.only',
                ],
                'default_value' => 'none',
            ],
        ],
        'correspondence_via'     => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    'none'  => 'module::field.correspondence_via.option.none',
                    'email' => 'module::field.correspondence_via.option.email',
                    'mail'  => 'module::field.correspondence_via.option.mail',
                ],
                'default_value' => 'none',
            ],
        ],
        'specials'               => 'anomaly.field_type.textarea',
        'signup_date'            => [
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'default_value' => null,
                'mode'          => 'date',
                'date_format'   => 'd-m-Y',
                'year_range'    => '-100:0',
                'picker'        => true,
            ],
        ],

    ];

}
