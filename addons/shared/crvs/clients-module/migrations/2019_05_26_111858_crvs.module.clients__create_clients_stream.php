<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class CrvsModuleClientsCreateClientsStream extends Migration
{

    /**
     * This migration creates the stream.
     * It should be deleted on rollback.
     *
     * @var bool
     */
    protected $delete = true;

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'clients',
        'title_column' => 'firstname',
        'translatable' => false,
        'versionable'  => false,
        'trashable'    => false,
        'searchable'   => true,
        'sortable'     => true,
    ];


    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'advisor'     => [
            'required' => true,
        ],
        'firstname'   => [
            'required' => true,
        ],
        'lastname'    => [
            'required' => true,
        ],
        'initials',
        'maiden_name',
        'gender'      => [
            'required' => true,
        ],
        'birthdate',
        'unknown_birthdate',
        'address',
        'housenumber',
        'housenumber_suffix',
        'postcode',
        'city',
        'nationality' => [
            'required' => true,
        ],

        'marital_status'       => [
            'required' => true,
        ],
        'phone_number',
        'mobile_number',
        'email',
        'bank_account_number',
        'has_wmo_dossier',
        'receive_correspondence',
        'correspondence_about' => [
            'required' => true,
        ],
        'correspondence_via'   => [
            'required' => true,
        ],
        'specials',
        'signup_date'          => [
            'required' => true,
        ],
    ];
}
