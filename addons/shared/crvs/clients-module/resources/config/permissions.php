<?php

return [
    'persons' => [
        'read',
        'write',
        'delete',
    ],
    'clients' => [
        'read',
        'write',
        'delete',
    ],
];
