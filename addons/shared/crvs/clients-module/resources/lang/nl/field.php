<?php

//        'label'        => 'Is this user enabled?',
//        'instructions' => 'The user will not be able to login or activate if disabled.',

return [
    'name'                => [ 'name' => 'Naam', ],
    'advisor'                => [ 'name' => 'Adviseur', ],
    'firstname'              => [ 'name' => 'Voornaam', ],
    'lastname'               => [ 'name' => 'Achternaam', ],
    'initials'               => [ 'name' => 'Voorletters', ],
    'maiden_name'            => [ 'name' => 'Meisjesnaam', ],
    'gender'                 => [
        'name'   => 'Geslacht',
        'option' => [
            'male'   => 'Man',
            'female' => 'Vrouw',
        ],
    ],
    'birthdate'              => [ 'name' => 'Geboortedatum', ],
    'unknown_birthdate'      => [ 'name' => 'Geboortedatum onbekend', ],
    'address'                => [ 'name' => 'Adres', ],
    'housenumber'            => [ 'name' => 'Huisnummer', ],
    'housenumber_suffix'     => [ 'name' => 'Huisnummer toevoeging', ],
    'postcode'               => [ 'name' => 'Postcode', ],
    'city'                   => [ 'name' => 'Plaats', ],
    'nationality'            => [
        'name'   => 'Nationaliteit',
        'option' => [
            'nederlandse' => 'Nederlandse',
            'turks'       => 'Turks',
            'overige'     => 'Overige',
        ],
    ],
    'marital_status'         => [
        'name'   => 'Burgelijke staat',
        'option' => [
            'single'      => 'Alleenstaand',
            'married'     => 'Gehuwd',
            'not_married' => 'Ongehuwd',
            'partnership' => 'Partnerschap',
            'divorced'    => 'Partnerschap beindigd / gescheiden',
            'widow'       => 'Weduwe / Weduwenaar',
        ],
    ],
    'phone_number'           => [ 'name' => 'Telefoon nummer' ],
    'mobile_number'          => [ 'name' => 'Mobiel' ],
    'email'                  => [ 'name' => 'Email' ],
    'bank_account_number'    => [ 'name' => 'Bankrekening' ],
    'has_wmo_dossier'        => [ 'name' => 'WMO Dossier' ],
    'receive_correspondence' => [ 'name' => 'Correspondentie ontvangen?' ],
    'correspondence_about'   => [
        'name'   => 'Zo ja, over?',
        'option' => [
            'none' => 'Selecteer',
            'all'  => 'Alle informatie over mantelzorg',
            'only' => 'Alleen informatie over mantelzorgwaardering',
        ],
    ],
    'correspondence_via'     => [
        'name'   => 'Correspondentie Via',
        'option' => [
            'none'  => 'Geen enkele manier',
            'email' => 'Email',
            'mail'  => 'Reguliere post',
        ],
    ],
    'specials'               => [ 'name' => 'Bijzonderheden' ],
    'signup_date'            => [ 'name' => 'Datum aanmelding', ],
];
