<?php namespace Crvs\ClientsModule\Client;

use Anomaly\Streams\Platform\Entry\EntryCollection;

/**
 * Crvs\ClientsModule\Client\ClientCollection
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface[] all()
 */
/**
 * Crvs\ClientsModule\Client\ClientCollection
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface find($key, $default=null)
 */
/**
 * Crvs\ClientsModule\Client\ClientCollection
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface findBy($key, $value)
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface[] all()
 */
class ClientCollection extends EntryCollection
{

}
