<?php namespace Crvs\ClientsModule\Client;

use Crvs\ClientsModule\Client\Contract\ClientInterface;
use Anomaly\Streams\Platform\Model\Clients\ClientsClientsEntryModel;

/**
 * Crvs\ClientsModule\Client\ClientModel
 *
 * @property int $id
 * @property int|null $sort_order
 * @property \Illuminate\Support\Carbon $created_at
 * @property int|null $created_by_id
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $updated_by_id
 * @property int $advisor_id
 * @property string $firstname
 * @property string $lastname
 * @property string|null $initials
 * @property string|null $maiden_name
 * @property string $gender
 * @property \Illuminate\Support\Carbon|null $birthdate
 * @property int|null $unknown_birthdate
 * @property string|null $address
 * @property string|null $housenumber
 * @property string|null $housenumber_suffix
 * @property string|null $postcode
 * @property string|null $city
 * @property string $nationality
 * @property string $marital_status
 * @property string|null $phone_number
 * @property string|null $mobile_number
 * @property string|null $email
 * @property int|null $bank_account_number
 * @property int|null $has_wmo_dossier
 * @property int|null $receive_correspondence
 * @property string $correspondence_about
 * @property string $correspondence_via
 * @property string|null $specials
 * @property \Illuminate\Support\Carbon $signup_date
 * @property \Anomaly\UsersModule\User\UserModel $advisor
 * @property \Anomaly\UsersModule\User\UserModel|null $createdBy
 * @property mixed|null $raw
 * @property \Anomaly\UsersModule\User\UserModel|null $updatedBy
 * @property \Anomaly\Streams\Platform\Version\VersionCollection|\Anomaly\Streams\Platform\Version\VersionModel[] $versions
 * @method static \Anomaly\Streams\Platform\Entry\EntryQueryBuilder|\Crvs\ClientsModule\Client\ClientModel newModelQuery()
 * @method static \Anomaly\Streams\Platform\Entry\EntryQueryBuilder|\Crvs\ClientsModule\Client\ClientModel newQuery()
 * @method static \Anomaly\Streams\Platform\Entry\EntryQueryBuilder|\Crvs\ClientsModule\Client\ClientModel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Anomaly\Streams\Platform\Entry\EntryModel sorted($direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\Anomaly\Streams\Platform\Model\EloquentModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\Anomaly\Streams\Platform\Model\EloquentModel translatedIn($locale)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereAdvisorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereBankAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereBirthdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereCorrespondenceAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereCorrespondenceVia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereCreatedById($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereHasWmoDossier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereHousenumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereHousenumberSuffix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereInitials($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereMaidenName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereMaritalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereMobileNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereNationality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereReceiveCorrespondence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereSignupDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereSpecials($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereUnknownBirthdate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Crvs\ClientsModule\Client\ClientModel whereUpdatedById($value)
 * @mixin \Eloquent
 */
class ClientModel extends ClientsClientsEntryModel implements ClientInterface
{

}
