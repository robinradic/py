<?php namespace Crvs\ClientsModule\Client;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

/**
 * Crvs\ClientsModule\Client\ClientPresenter
 *
 * @mixin \Crvs\ClientsModule\Client\ClientModel
 * @property \Crvs\ClientsModule\Client\ClientModel $object
 */
class ClientPresenter extends EntryPresenter
{
    public function name()
    {
        $initials = $this->object->initials;
        if ( ! $initials || $initials === '') {
            $initials = $this->object->firstname[ 0 ];
        }
        $initials = strtoupper($initials);

        $displayName = $this->object->lastname;
        $displayName .= '. ';
        $displayName .= $this->aanhef();
        $displayName .= $initials;

        return $displayName;
    }

    public function aanhef()
    {
        return $this->object->gender === 'male' ? 'Dhr' : 'Mw';
    }
}
