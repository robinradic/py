<?php namespace Crvs\ClientsModule\Client;

use Crvs\ClientsModule\Client\Contract\ClientRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

/**
 * Crvs\ClientsModule\Client\ClientRepository
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface first($direction = 'asc')
 */
/**
 * Crvs\ClientsModule\Client\ClientRepository
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface find($id)
 */
/**
 * Crvs\ClientsModule\Client\ClientRepository
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface findBy($key, $value)
 */
/**
 * Crvs\ClientsModule\Client\ClientRepository
 *
 * @method \Crvs\ClientsModule\Client\ClientCollection|\Crvs\ClientsModule\Client\Contract\ClientInterface[] findAllBy($key, $value)
 */
/**
 * Crvs\ClientsModule\Client\ClientRepository
 *
 * @method \Crvs\ClientsModule\Client\ClientCollection|\Crvs\ClientsModule\Client\Contract\ClientInterface[] findAll(array $ids)
 */
/**
 * Crvs\ClientsModule\Client\ClientRepository
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface create(array $attributes)
 */
/**
 * Crvs\ClientsModule\Client\ClientRepository
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface getModel()
 */
/**
 * Crvs\ClientsModule\Client\ClientRepository
 *
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface newInstance(array $attributes = [])
 * @method \Crvs\ClientsModule\Client\Contract\ClientInterface first($direction = 'asc')
 */
class ClientRepository extends EntryRepository implements ClientRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ClientModel
     */
    protected $model;

    /**
     * Create a new ClientRepository instance.
     *
     * @param ClientModel $model
     */
    public function __construct(ClientModel $model)
    {
        $this->model = $model;
    }
}
