<?php namespace Crvs\ClientsModule\Client\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Crvs\ClientsModule\Client\Contract\ClientInterface
 *
 * @mixin \Crvs\ClientsModule\Client\ClientModel
 */
interface ClientInterface extends EntryInterface
{

}
