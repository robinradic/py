<?php namespace Crvs\ClientsModule\Client\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Crvs\ClientsModule\Client\Contract\ClientRepositoryInterface
 *
 * @mixin \Crvs\ClientsModule\Client\ClientRepository
 */
interface ClientRepositoryInterface extends EntryRepositoryInterface
{

}
