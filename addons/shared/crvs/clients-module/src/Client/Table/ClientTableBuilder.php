<?php namespace Crvs\ClientsModule\Client\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class ClientTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'search' => [
            'filter' => 'search',
            'fields' => [
                'name',
                'firstname',
                'address',
                'postcode',
                'city',
                'phone_number',
            ],
        ],
        'firstname',
        'address',
        'postcode',
        'city',
        'phone_number',
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'name' => [
            'value' => 'entry.name',
        ],
        'firstname',
        'address',
        'postcode',
        'city',
        'phone_number',
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
//        'edit',
        'details' => [
            'text' => '',
            'icon' => 'fa fa-info',
            'type' => 'info',
        ],
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete',
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
