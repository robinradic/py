<?php

namespace Crvs\ClientsModule;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Crvs\ClientsModule\Traits\SeedsClients;
use Faker\Factory;


class ClientsModuleSeeder extends Seeder
{
    use SeedsClients;

    /** @var \Faker\Generator */
    protected $faker;

    public function __construct()
    {
        parent::__construct();
        $this->faker = Factory::create('nl_NL');
    }

    public function run()
    {
        $this->createClient([ 'firstname' => 'Bert', 'lastname' => 'Breukhoven', 'initials' => 'B.' ]);
        $this->createClient([ 'firstname' => $this->faker->firstName, 'lastname' => $this->faker->lastName, ]);
        $this->createClient([ 'firstname' => 'Egbert', 'lastname' => 'Egbertsten', ]);
        $this->createClient([ 'firstname' => 'Gerda', 'lastname' => 'Gerardsen', ]);
        $this->createClient([ 'firstname' => 'Karel', 'lastname' => 'Karelsen', ]);
        $this->createClient([ 'firstname' => 'Wim', 'lastname' => 'Klein', ]);
    }

}
