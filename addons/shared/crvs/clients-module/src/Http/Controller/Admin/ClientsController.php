<?php namespace Crvs\ClientsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Crvs\ClientsModule\Client\Form\ClientFormBuilder;
use Crvs\ClientsModule\Client\Table\ClientTableBuilder;

class ClientsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ClientTableBuilder $table
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ClientTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ClientFormBuilder $form
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ClientFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ClientFormBuilder $form
     * @param                   $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ClientFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    public function details(ClientFormBuilder $form, $id)
    {
        $data = view('module::details', [
            'form' => $form
                ->setLocked(true)
                ->setReadOnly(true)
                ->setSave(false)
                ->setFormMode('asdf')
                ->setEntry($id)
//                ->setOption('wrapper_view', false)
//                ->setOption('form_view', 'module::details-form')
//                ->setOption('class', 'col-md-5'),
        ]);


        return $data;
    }
}
