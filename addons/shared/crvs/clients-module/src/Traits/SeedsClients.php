<?php


namespace Crvs\ClientsModule\Traits;


use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Crvs\ClientsModule\Client\Contract\ClientRepositoryInterface;
use Faker\Factory;

trait SeedsClients
{
    /** @return \Crvs\ClientsModule\Client\Contract\ClientInterface */
    protected function createClient(array $data)
    {
        $userRepository   = app()->make(UserRepositoryInterface::class);
        $clientRepository = app()->make(ClientRepositoryInterface::class);
        $faker            = Factory::create('nl_NL');

        $correspondence = $faker->boolean;
        $defaults       = [
            'gender'                 => $faker->randomElement([ 'male', 'female' ]),
            'birthdate'              => $faker->boolean(70) ? $faker->date('d-m-Y') : null,
            'unknown_birthdate'      => false,
            'address'                => $faker->address,
            'housenumber'            => $faker->numberBetween(0, 300),
            'housenumber_suffix'     => $faker->boolean(30) ? $faker->randomLetter : null,
            'postcode'               => $faker->postcode,
            'city'                   => $faker->city,
            'nationality'            => $faker->randomElement([ 'nederlandse', 'turks', 'overige' ]),
            'marital_status'         => $faker->randomElement([ 'single', 'married', 'not_married', 'partnership', 'divorced', 'widow' ]),
            'phone_number'           => $faker->phoneNumber,
            'mobile_number'          => $faker->phoneNumber,
            'email'                  => $faker->email,
            'bank_account_number'    => $faker->bankAccountNumber,
            'has_wmo_dossier'        => $faker->boolean,
            'receive_correspondence' => $correspondence,
            'correspondence_about'   => $correspondence ? $faker->randomElement([ 'all', 'only' ]) : 'none',
            'correspondence_via'     => $correspondence ? $faker->randomElement([ 'email', 'mail' ]) : 'none',
            'specials'               => $faker->boolean(30) ? $faker->paragraph(10) : '',
            'signup_date'            => $faker->date('d-m-Y'),
        ];
        $data           = array_replace($defaults, $data);
        if ( ! is_string($data[ 'birthdate' ]) || $data[ 'birthdate' ] === '') {
            $data[ 'unknown_birthdate' ] = true;
        }

        $client  = $clientRepository->create($data);
        $advisor = $userRepository->findByUsername('lisa');
        if ($advisor) {
            $client->advisor = $advisor;
            $clientRepository->save($client);
        }
        return $client;
    }

}
