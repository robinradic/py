<?php

namespace Crvs\CoreModule;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Crvs\CoreModule\Traits\SeedsRoles;
use Crvs\CoreModule\Traits\SeedsUsers;
use Faker\Factory;

class CoreModuleSeeder extends Seeder
{
    use SeedsRoles;
    use SeedsUsers;

    /** @var \Faker\Generator */
    protected $faker;

    public function __construct()
    {
        parent::__construct();
        $this->faker = Factory::create('nl_NL');
    }

    public function run()
    {
        $serviceDeskRole = $this->createRole('Service Desk', 'service_desk');
        $this->createUser('lisa', [ $serviceDeskRole ]);
    }
}
