<?php /** @noinspection PropertyInitializationFlawsInspection */

namespace Crvs\CoreModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Addon\Event\AddonsHaveRegistered;
use Anomaly\Streams\Platform\Asset\Asset;
use Anomaly\Streams\Platform\Event\Ready;
use Anomaly\Streams\Platform\View\Event\ViewComposed;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\Router;

class CoreModuleServiceProvider extends AddonServiceProvider
{
    protected $plugins = [];

    protected $commands = [];

    protected $schedules = [];

    protected $api = [];

    protected $routes = [];

    protected $middleware = [
        //Crvs\CoreModule\Http\Middleware\ExampleMiddleware::class
    ];

    protected $groupMiddleware = [
        //'web' => [
        //    Crvs\CoreModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    protected $routeMiddleware = [];

    protected $listeners = [
        //Crvs\CoreModule\Event\ExampleEvent::class => [
        //    Crvs\CoreModule\Listener\ExampleListener::class,
        //],
    ];

    protected $aliases = [
        //'Example' => Crvs\CoreModule\Example::class
    ];

    protected $bindings = [];

    protected $singletons = [];

    protected $providers = [];

    protected $overrides = [];

    public function register()
    {
    }

    /**
     * Boot the addon.
     */
    public function boot(Dispatcher $events, Factory $view)
    {
        $events->listen(AddonsHaveRegistered::class, function (AddonsHaveRegistered $event) use ($view) {
            $view->getFinder()->prependNamespace('theme', [ $this->addon->getPath('resources/views/admin/'), ]); //            $finder->prependNamespace('pyrocms.theme.accelerant', [$this->addon->getPath('resources/views/admin/'),]);
        });

    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
