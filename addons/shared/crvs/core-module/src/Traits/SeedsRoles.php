<?php

namespace Crvs\CoreModule\Traits;

use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;

trait SeedsRoles
{
    /** @return \Anomaly\UsersModule\Role\Contract\RoleInterface */
    public function createRole(string $name, string $slug, ?string $description = null, ?array $permissions = null)
    {
        $roleRepository = app()->make(RoleRepositoryInterface::class);
        $role = $roleRepository->findBySlug($slug);
        if ($role !== null) {
            return $role;
        }
        return $roleRepository->create(compact('name', 'slug', 'description', 'permissions'));
    }
}
