<?php

namespace Crvs\CoreModule\Traits;

use Anomaly\UsersModule\Role\Contract\RoleInterface;
use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Anomaly\UsersModule\User\UserActivator;

trait SeedsUsers
{

    /** @return \Anomaly\UsersModule\User\Contract\UserInterface */
    public function createUser(string $username, array $roles = [], array $data = [])
    {
        $userRepository = app()->make(UserRepositoryInterface::class);
        $activator      = app()->make(UserActivator::class);
        $roleRepository = app()->make(RoleRepositoryInterface::class);

        $user = $userRepository->findByUsername($username);
        if ($user === null) {
            $defaults = [
                'username'     => $username,
                'email'        => $username . '@test.com',
                'password'     => bcrypt('test'),
                'first_name'   => ucfirst($username),
                'display_name' => ucfirst($username),
            ];
            $data     = array_replace($defaults, $data);
            /** @var \Anomaly\UsersModule\User\Contract\UserInterface $user */
            $user = $userRepository->create($data);
        }

        /** @var integer[] $roles */
        $roles = collect($roles)->map(function ($role) use ($roleRepository) {
            if (is_string($role)) {
                $role = $roleRepository->findBySlug($role);
            }
            if (is_int($role)) {
                $role = $roleRepository->find($role);
            }
            if ($role instanceof RoleInterface) {
                return $role->getId();
            }
            return $role;
        })->all();

        $user->roles()->sync($roles);
        $activator->force($user);

        return $user;
    }

}
