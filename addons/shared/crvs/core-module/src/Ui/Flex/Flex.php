<?php

namespace Crvs\CoreModule\Ui\Flex;

use Anomaly\Streams\Platform\Model\EloquentModel;
use Anomaly\Streams\Platform\Stream\Contract\StreamInterface;
use Anomaly\Streams\Platform\Ui\Grid\Component\Item\ItemCollection;
use Anomaly\Streams\Platform\Ui\Grid\Contract\GridRepositoryInterface;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class Flex
{
    /**
     * The grid content.
     *
     * @var null|string
     */
    protected $content = null;

    /**
     * The grid response.
     *
     * @var null|Response
     */
    protected $response = null;

    /**
     * The grid data.
     *
     * @var Collection
     */
    protected $data;

    /**
     * The grid items.
     *
     * @var ItemCollection
     */
    protected $containers;

    /**
     * The grid entries.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $entries;

    /**
     * The grid options.
     *
     * @var Collection
     */
    protected $options;

    /**
     * Create a new Grid instance.
     *
     * @param Collection     $data
     * @param Collection     $options
     * @param Collection     $entries
     * @param Collection     $headers
     * @param ItemCollection $items
     */
    public function __construct(
        Collection $data,
        Collection $options,
        Collection $entries,
        Collection $headers,
        ItemCollection $items
    ) {
        $this->data       = $data;
        $this->containers = $items;
        $this->entries    = $entries;
        $this->headers    = $headers;
        $this->options    = $options;
    }
}
