<?php

namespace Crvs\CoreModule\Ui\Flex;

use Anomaly\Streams\Platform\Traits\FiresCallbacks;
use Anomaly\Streams\Platform\Ui\Grid\Command\AddAssets;
use Anomaly\Streams\Platform\Ui\Grid\Command\BuildGrid;
use Anomaly\Streams\Platform\Ui\Grid\Command\LoadGrid;
use Anomaly\Streams\Platform\Ui\Grid\Command\MakeGrid;
use Anomaly\Streams\Platform\Ui\Grid\Command\PostGrid;
use Anomaly\Streams\Platform\Ui\Grid\Command\SetGridResponse;
use Anomaly\Streams\Platform\Ui\Grid\Grid;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Response;

class FlexBuilder
{

    use FiresCallbacks;
    use DispatchesJobs;

    /**
     * The grid model.
     *
     * @var null|string
     */
    protected $model = null;

    /**
     * The buttons configuration.
     *
     * @var array|string
     */
    protected $containers = [];

    /**
     * The grid options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The grid assets.
     *
     * @var array
     */
    protected $items = [];

    /** @var \Crvs\CoreModule\Ui\Flex\Flex */
    protected $flex;

    public function __construct(Flex $flex)
    {
        $this->flex = $flex;
    }

    /**
     * Build the grid.
     */
    public function build()
    {
        $this->fire('ready', ['builder' => $this]);

//        $this->dispatchNow(new BuildGrid($this));

        if (app('request')->isMethod('post')) {
//            $this->dispatchNow(new PostGrid($this));
        }
    }

    /**
     * Make the grid response.
     */
    public function make()
    {
        $this->build();

        if (!app('request')->isMethod('post')) {
//            $this->dispatchNow(new LoadGrid($this));
//            $this->dispatchNow(new AddAssets($this));
//            $this->dispatchNow(new MakeGrid($this));
        }
    }

    /**
     * Render the grid.
     *
     * @return Response
     */
    public function render()
    {
        $this->make();

//        $this->dispatchNow(new SetGridResponse($this));

        return $this->flex->getResponse();
    }

    public function getFlex()
    {
        return $this->flex;
    }
}
