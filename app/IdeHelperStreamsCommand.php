<?php

namespace App;

use Anomaly\Streams\Platform\Addon\AddonCollection;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Barryvdh\Reflection\DocBlock;
use Barryvdh\Reflection\DocBlock\Context;
use Barryvdh\Reflection\DocBlock\Serializer as DocBlockSerializer;
use Barryvdh\Reflection\DocBlock\Tag;
use Illuminate\Console\Command;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Console\Input\InputOption;

class IdeHelperStreamsCommand extends Command
{
    protected $name = 'ide-helper:streams';

    protected $description = 'Generate autocompletion for stream classes';

    public function handle(StreamRepositoryInterface $streams, AddonCollection $addons)
    {
        $ignoreCoreAddons   = $this->option('core') !== true;
        $ignoreSharedAddons = $this->option('shared') !== true;

        foreach ($addons as $addon) {
            /** @var \Anomaly\Streams\Platform\Addon\Module\Module $addon */
            if ($ignoreCoreAddons && $addon->isCore()) {
                continue;
            }
            if ($ignoreSharedAddons && $addon->isShared()) {
                continue;
            }
            if ($addon->getType() === 'module') {
                foreach ($streams->findAllByNamespace($addon->getSlug()) as $stream) {
                    /** @var \Anomaly\Streams\Platform\Stream\Contract\StreamInterface $stream */
                    $s         = $stream->toArray();
                    $name      = ucfirst(camel_case(str_singular($stream->getSlug())));
                    $path      = $addon->getPath('src/' . $name);
                    $namespace = with(new ReflectionClass($addon))->getNamespaceName();
                    if (is_dir($path)) {
                        $this->comment("Generating PHPDoc for stream [{$stream->getNamespace()}.{$stream->getSlug()}]");
                        $this->generateStreamPhpDocs($path, $namespace);
                    }
                }
            }
        }
    }

    protected function getOptions()
    {
        return [
            [ 'core', null, InputOption::VALUE_NONE, 'Include core addons?' ],
            [ 'shared', null, InputOption::VALUE_NONE, 'Include shared addons?' ],
        ];
    }

    protected function generateStreamPhpDocs($path, $namespace)
    {
        $name                     = path_get_directory_name($path);
        $modelClass               = "\\{$namespace}\\{$name}\\{$name}Model";
        $collectionClass          = "\\{$namespace}\\{$name}\\{$name}Collection";
        $criteriaClass            = "\\{$namespace}\\{$name}\\{$name}Criteria";
        $observerClass            = "\\{$namespace}\\{$name}\\{$name}Observer";
        $presenterClass           = "\\{$namespace}\\{$name}\\{$name}Presenter";
        $repositoryClass          = "\\{$namespace}\\{$name}\\{$name}Repository";
        $routerClass              = "\\{$namespace}\\{$name}\\{$name}Router";
        $seederClass              = "\\{$namespace}\\{$name}\\{$name}Seeder";
        $interfaceClass           = "\\{$namespace}\\{$name}\\Contract\\{$name}Interface";
        $repositoryInterfaceClass = "\\{$namespace}\\{$name}\\Contract\\{$name}RepositoryInterface";

        $this->ensureTag($interfaceClass, 'mixin', $modelClass);
        $this->ensureTag($repositoryInterfaceClass, 'mixin', $repositoryClass);
        $this->ensureTag($presenterClass, 'mixin', $modelClass);

        $this->ensureTag($repositoryClass, 'method', "{$interfaceClass} first(\$direction = 'asc')");
        $this->ensureTag($repositoryClass, 'method', "{$interfaceClass} find(\$id)");
        $this->ensureTag($repositoryClass, 'method', "{$interfaceClass} findBy(\$key, \$value)");
        $this->ensureTag($repositoryClass, 'method', "{$collectionClass}|{$interfaceClass}[] findAllBy(\$key, \$value)");
        $this->ensureTag($repositoryClass, 'method', "{$collectionClass}|{$interfaceClass}[] findAll(array \$ids)");
        $this->ensureTag($repositoryClass, 'method', "{$interfaceClass} create(array \$attributes)");
        $this->ensureTag($repositoryClass, 'method', "{$interfaceClass} getModel()");
        $this->ensureTag($repositoryClass, 'method', "{$interfaceClass} newInstance(array \$attributes = [])");

        $this->ensureTag($collectionClass, 'method', "{$interfaceClass}[] all()");
        $this->ensureTag($collectionClass, 'method', "{$interfaceClass} find(\$key, \$default=null)");
        $this->ensureTag($collectionClass, 'method', "{$interfaceClass} findBy(\$key, \$value)");

    }

    protected function ensureTag(string $class, string $tagName, string $tagContent)
    {
        try {
            $reflection  = new ReflectionClass($class);
            $originalDoc = $reflection->getDocComment();
            $phpdoc      = new DocBlock($reflection, new Context($reflection->getNamespaceName()));
            $content     = collect($phpdoc->getTagsByName($tagName))->map->getContent();
            $hasTag      = $content->filter(function ($content) use ($tagContent) {
                    return str_contains($content, $tagContent);
                })->count() > 0;

            if ( ! $hasTag) {
                $phpdoc->appendTag(Tag::createInstance('@' . $tagName . ' ' . $tagContent, $phpdoc));
                if ( ! $phpdoc->getText()) {
                    $phpdoc->setText($reflection->getName());
                }
                $this->writeDocBlock($reflection, $phpdoc, $originalDoc);
            }
        }
        catch (ReflectionException $e) {
            // ignore
        }
    }


    protected function writeDocBlock(ReflectionClass $reflection, DocBlock $phpdoc, $originalDoc = null)
    {
        $serializer = new DocBlockSerializer();
        $serializer->getDocComment($phpdoc);
        $docComment = $serializer->getDocComment($phpdoc);

        $classname = $reflection->getShortName();
        $filename  = $reflection->getFileName();
        $type      = 'class';
        if ($reflection->isInterface()) {
            $type = 'interface';
        }
        $contents = file_get_contents($filename);

        if ($originalDoc) {
            $contents = str_replace($originalDoc, $docComment, $contents);
        } else {
            $needle  = "{$type} {$classname}";
            $replace = "{$docComment}\n{$type} {$classname}";
            $pos     = strpos($contents, $needle);
            if ($pos !== false) {
                $contents = substr_replace($contents, $replace, $pos, strlen($needle));
            }
        }
        if (file_put_contents($filename, $contents)) {
            $this->info('Written new phpDocBlock to ' . $filename);
        }
    }

}
