<?php

namespace App;

use Illuminate\Console\Command;
use Laradic\Filesystem\Filesystem;
use LSS\Array2XML;
use LSS\XML2Array;

class IdeaCommand extends Command
{
    protected $signature = 'idea';

    /**
     * handle method
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $this->handleSourceFolders();
    }

    protected function handleSourceFolders()
    {
        $urls                 = collect(Filesystem::create()->globule(dirname(__DIR__) . '/{addons/shared,core}/*/*/src'))->map(function ($path) {
            return str_replace_first(dirname(__DIR__) . '/', '', $path);
        });
        $imlFilePath          = head(glob(dirname(__DIR__) . '/.idea/*.iml', GLOB_NOSORT));
        $data                 = XML2Array::createArray(file_get_contents($imlFilePath));
        $currentUrls = collect($data[ 'module' ][ 'component' ][ 'content' ][ 'sourceFolder' ])->map(function ($sourceFolder) {
            return $sourceFolder[ '@attributes' ][ 'url' ];
        });
        $urlPrefix            = $data[ 'module' ][ 'component' ][ 'content' ][ '@attributes' ][ 'url' ];
        $foundUrls   = $urls->map(function ($url) use ($urlPrefix) {
            return path_join([ $urlPrefix, $url ]);
        });
        $missingUrls = $foundUrls->diff($currentUrls);

        foreach ($missingUrls as $url) {
            $segments      = array_reverse(explode('/', dirname($url)));
            $slug          = array_shift($segments);
            $vendor        = array_shift($segments);
            $packagePrefix = studly_case($vendor) . '\\' . studly_case($slug);

            $data[ 'module' ][ 'component' ][ 'content' ][ 'sourceFolder' ][] = [
                '@value'      => '',
                '@attributes' => [
                    'url'           => $url,
                    'isTestSource'  => 'false',
                    'packagePrefix' => $packagePrefix,
                ],
            ];
            $url = str_remove_left($url, $urlPrefix . '/');
            $this->line(" - Added source folder [{$url}] for namespace [{$packagePrefix}]");
        }

        $xml = Array2XML::createXML('module', $data[ 'module' ])->saveXML();
        file_put_contents($imlFilePath, $xml);
    }

    protected function handlePhpIncludePaths()
    {
        $paths       = collect(Filesystem::create()->globule(dirname(__DIR__) . '/core/*/*/src'))->map(function ($path) {
            return str_replace_first(dirname(__DIR__), '$PROJECT_DIR$', $path);
        });
        $xmlFilePath = dirname(__DIR__) . '/.idea/php.xml';
        if ( ! file_exists($xmlFilePath)) {
            return $this->error("Could not find xml file [{$xmlFilePath}]");
        }
        $data              = XML2Array::createArray(file_get_contents($xmlFilePath));
        $getComponentIndex = static function (string $name, array $components) {
            foreach ($components as $index => $component) {
                if (data_get($component, '@attributes.name') === $name) {
                    return $index;
                }
            }
        };
        $componentIndex    = $getComponentIndex('PhpIncludePathManager', $data[ 'project' ][ 'component' ]);
        $includePaths      = collect($data[ 'project' ][ 'component' ][ $componentIndex ][ 'include_path' ][ 'path' ])->map(function ($path) {
            return $path[ '@attributes' ][ 'value' ];
        });
        $missingPaths      = $paths->diff($includePaths);

        if ($missingPaths->count() === 0) {
            return $this->info('Not missing any paths.');
        }

        $addPaths = $missingPaths->map(function ($path) {
            $this->line(" - Adding PHP include path: {$path}");
            return [ '@value' => '', '@attributes' => [ 'value' => $path ] ];
        });
        foreach ($addPaths as $addPath) {
            $data[ 'project' ][ 'component' ][ $componentIndex ][ 'include_path' ][ 'path' ][] = $addPath;
        }

        $xml = Array2XML::createXML('project', $data[ 'project' ])->saveXML();
        file_put_contents($xmlFilePath, $xml);
    }
}
