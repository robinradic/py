<?php

namespace App\Overrides;

use Anomaly\MultipleFieldType\MultipleFieldType;
use Anomaly\PolymorphicFieldType\PolymorphicFieldType;
use Anomaly\RelationshipFieldType\RelationshipFieldType;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentInterface;
//use Pywow\RelatedFieldType\RelatedFieldType;

class FieldTypeParser
{

    /**
     * Return the parsed relation.
     *
     * @param AssignmentInterface $assignment
     *
     * @return string
     */
    public function relation(AssignmentInterface $assignment)
    {
        $fieldSlug      = $assignment->getFieldSlug();
        $returns        = '\Illuminate\Database\Eloquent\Relations\Relation';
        $relationMethod = 'belongsTo';
        if ($assignment->getFieldType() instanceof RelationshipFieldType) {
            $returns        = '\Illuminate\Database\Eloquent\Relations\BelongsTo';
            $relationMethod = 'belongsTo';
        } elseif ($assignment->getFieldType() instanceof MultipleFieldType) {
            $returns        = '\Illuminate\Database\Eloquent\Relations\HasMany';
            $relationMethod = 'HasMany';
        } elseif ($assignment->getFieldType() instanceof PolymorphicFieldType) {
            $returns        = '\Illuminate\Database\Eloquent\Relations\MorphTo';
            $relationMethod = 'morphTo';
        }
//        } elseif ($assignment->getFieldType() instanceof RelatedFieldType) {
//            $returns        = '\Illuminate\Database\Eloquent\Relations\hasMany';
//            $relationMethod = 'hasMany';
//        }
        $fieldName = str_humanize($fieldSlug);
        $method    = camel_case($fieldSlug);

        return "
    /**
     * The {$fieldName} relation
     *
     * @return {$returns}
     */
    public function {$method}()
    {
        // \$this->{$relationMethod}()
        return \$this->getFieldType('{$fieldSlug}')->getRelation();
    }
";
    }
}
