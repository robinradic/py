<?php

namespace App\Overrides;

class ModelsCommand extends \Barryvdh\LaravelIdeHelper\Console\ModelsCommand
{
    protected function setProperty($name, $type = null, $read = null, $write = null, $comment = '', $nullable = false)
    {
        return parent::setProperty($name, $type, true, true, $comment, $nullable);
    }

    protected function generateDocs($loadModels, $ignore = '')
    {
        $ignore = implode(',', array_merge(explode(',', $ignore), [
            \Anomaly\UsersModule\User\Table\Filter\StatusFilterQuery::class
        ]));
        return parent::generateDocs($loadModels, $ignore);
    }

}
