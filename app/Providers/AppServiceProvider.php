<?php

namespace App\Providers;

use Anomaly\Streams\Platform\Addon\FieldType\FieldTypeParser;
use App\IdeaCommand;
use App\IdeHelperStreamsCommand;
use App\Overrides\ModelsCommand;
use App\TestCommand;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('command.test', function () {
            return new TestCommand();
        });
        $this->app->extend('command.ide-helper.models', function () {
            return new ModelsCommand($this->app[ 'files' ]);
        });
        $this->app->singleton('command.ide-helper.streams', function () {
            return new IdeHelperStreamsCommand();
        });
        $this->app->singleton('command.idea', function () {
            return new IdeaCommand();
        });
        $this->commands([ 'command.test', 'command.ide-helper.models', 'command.ide-helper.streams', 'command.idea' ]);

        $this->app->bind(FieldTypeParser::class, \App\Overrides\FieldTypeParser::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FieldTypeParser::class, \App\Overrides\FieldTypeParser::class);
    }
}
