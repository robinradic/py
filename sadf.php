<?php ///** @noinspection UnusedFunctionResultInspection */
//
//
//namespace CRUD {
//
//    class CRUD
//    {
//        public function field($type, $config)
//        {
//            get_instance()->db->insert('crud_fields', [
//
//            ]);
//            return new Field();
//        }
//
//        public function create($name)
//        {
//            return new Table();
//        }
//
//        public function table($name)
//        {
//            return new Table();
//        }
//    }
//
//    class Field
//    {
//
//    }
//
//    class Table
//    {
//        /** @var \CRUDForm */
//        public $form;
//
//        public function assign(array $assignments)
//        {
//            return $this;
//        }
//
//        public function disable(array $fieldNames)
//        {
//            return $this;
//        }
//
//        public function render(array $renderOptions)
//        {
//            return '';
//        }
//
//        public function assignment($name)
//        {
//            return new Assignment();
//        }
//    }
//
//    class Entries
//    {
//
//    }
//
//    class Form
//    {
//        public function render($id, array $renderOptions)
//        {
//            return '';
//        }
//    }
//
//    class Assignment
//    {
//        protected $options = [];
//
//        public function set($path, $value)
//        {
//            $this->options[ $path ] = $value;
//            return $this;
//        }
//
//        public function get($path, $defaultValue = null)
//        {
//            return $this->options[ $path ];
//        }
//    }
//}
//
//$lib = new CRUD\CRUD();
//$lib->field('text', [
//    'db_type' => 'varchar',
//    'view'    => 'path/to/text-field-view',
//]);
//$lib->field('number', [
//    'db_type' => 'integer',
//    'view'    => 'path/to/number-field-view',
//]);
//
//
//// client-module / module.php
//$table = $lib->create('client');
//$table->assign([
//    'name'    => [
//        'type'  => 'text',
//        'rules' => [ 'max' => 30, 'min' => 1, 'required' => true ],
//        'label' => 'Voornaam',
//        'save'  => function ($value) {
//            return $value;
//        },
//        'load'  => function () {
//            return 'Bert';
//        },
//    ],
//    'surname' => [
//        'type'  => 'text',
//        'rules' => [ 'max' => 30, 'min' => 1, 'required' => true ],
//        'label' => 'Achternaam',
//    ],
//]);
//$lib->create('client')->disable([ 'surname' ]);
//
//// client-module / controllers / client_controller->showClients()
//$table->render([
//    'columns' => [ 'name', 'surname' ],
//]);
//
//
//// client-module / controllers / client_controller->editClient($id)
//$table->form->render(3, [
//    'submit' => function ($data, $table) {
//        $table->form->submit($data);
//    },
//]);
//
//
//// application/hooks/ClientModuleHooks->loaded
//function loaded(CRUD $lib)
//{
//    $table = $lib->table('client');
//    $table->assign([
//        'age' => [
//            'type'  => 'number',
//            'rules' => [ 'min' => 10, 'max' => 150 ],
//            'label' => 'Leeftijd',
//        ],
//    ]);
//    $table
//        ->assignment('surname')
//        ->set('rules.required', false);
//}
